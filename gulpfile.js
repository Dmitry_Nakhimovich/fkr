var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'), 
    jshint = require("gulp-jshint"), 
    rigger = require('gulp-rigger'),
    prefixer = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    pug = require('gulp-pug'),
    cssmin = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    //imagemin = require('gulp-imagemin'),
    rimraf = require('rimraf'),
    plumber = require("gulp-plumber"),
    //pngquant = require('imagemin-pngquant'),
    connect = require('gulp-connect'); 
    util = require('gulp-util');
    //browserSync = require("browser-sync"),
    //reload = browserSync.reload;

var path = {
    build: {
        pug: 'dist/',
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/'
    },
    src: {
        pug: 'src/pug/*.pug',
        html: 'src/*.html',
        js: 'src/js/*.js',
        jshint: 'src/js/**/*.js',
        style: 'src/style/main.css',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        scripts: 'src/scripts/*.js',
        styles: 'src/style/other/*.css'
    },
    watch: {
        pug: 'src/pug/**/*.pug',
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.css',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './dist'
};

var config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Nakhimovich_server"
};

// gulp.task('webserver', function () {
//     browserSync(config);
// });

gulp.task('connect', function(){
    connect.server({ //настриваем конфиги сервера
        root: ['./dist'], //корневая директория запуска сервера
        port: 9000, //какой порт будем использовать
        livereload: true //инициализируем работу LiveReload
    });
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
   		.pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(connect.reload());
});

gulp.task('pug:build', function () {
    gulp.src(path.src.pug)
        .pipe(rigger())
        .pipe(pug(
                {   
                    pretty:true
                }
            ))
        .on('error', util.log)
        .pipe(gulp.dest(path.build.pug))
        .pipe(connect.reload());
});

gulp.task('jshint:build', function() {
    return gulp.src(path.src.jshint) //выберем файлы по нужному пути
        .pipe(jshint()) //прогоним через jshint
        .pipe(jshint.reporter('jshint-stylish')); //стилизуем вывод ошибок в консоль
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        //.pipe(rename({suffix: '.min'})) //добавим суффикс .min к выходному файлу
        .pipe(gulp.dest(path.build.js)) //выгрузим готовый файл в build
        .pipe(connect.reload()) //И перезагрузим сервер
});
// gulp.task('js:build', function () {
//     gulp.src(path.src.js)
//         .pipe(gulp.dest(path.build.js))
//         .pipe(reload({stream: true}));
// });

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe( postcss(
            [
                require('precss'),
                require('autoprefixer'),
                require('postcss-crip'),
                require('postcss-each'),
                require('postcss-for'),
                require('postcss-mixins'),
                require('postcss-nested'),
                require('postcss-nested-props')
            ]
        ) )
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(connect.reload())
        //.pipe(reload({stream: true}))
});

// gulp.task('image:build', function () {
//     gulp.src(path.src.img) //Выберем наши картинки
//         .pipe(imagemin({ //Сожмем их
//             progressive: true, //сжатие .jpg
//             svgoPlugins: [{removeViewBox: false}], //сжатие .svg
//             interlaced: true, //сжатие .gif
//             optimizationLevel: 5 //степень сжатия от 0 до 7
//         }))
//         .pipe(gulp.dest(path.build.img)) //выгрузим в build
//         .pipe(connect.reload()) //перезагрузим сервер
// });
gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки        
        .pipe(gulp.dest(path.build.img)) //выгрузим в build
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('scripts:build', function() {
    gulp.src(path.src.scripts)
        .pipe(gulp.dest(path.build.js))
});

gulp.task('styles:build', function() {
    gulp.src(path.src.styles)
     	.pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write()) //пропишем sourcemap
        .pipe(gulp.dest(path.build.css)) //выгрузим в build
        .pipe(connect.reload()) //перезагрузим сервер
        //.pipe(gulp.dest(path.build.css))
});

gulp.task('build', [
    //'html:build',
    'pug:build',
    'jshint:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build',
    'scripts:build',
    'styles:build'
]);


gulp.task('watch', function(){
    // watch([path.watch.html], function(event, cb) {
    //     gulp.start('html:build');
    // });
    watch([path.watch.pug], function(event, cb) {
        gulp.start('pug:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], ['jshint']);    
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'connect', 'watch']);
//gulp.task('default', ['build', 'webserver', 'watch']);