// --!-- function def =============================

// set minimum page height
function setMainHeight() {
  var mainHeight = $('main').height();
  var windowHeight = $(window).height();
  var windowWidth = $(window).width();
  var mainHeightNew = Math.max(windowHeight - $('header').height() - $('footer').height(), 0);
  $('main').css({
    height: ((mainHeight >= mainHeightNew) && (windowHeight > 700) && (windowWidth > 768)) ? mainHeight + 'px' : mainHeightNew + 'px'
  });
  console.log('main height: ', mainHeight, 'main height new: ', mainHeightNew);
}

// --!-- init plugins =============================
$(document).ready(function () {
  // resize blocks
  setMainHeight();
  $(window).resize(setMainHeight());
  // input tel mask init
  $("#inputPhone").mask("?+7 (999) 999-9999");

  // pages logic ==================================
  if ($(".index-page").length) {

  }

});