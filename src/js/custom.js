// all backend js here

$(document).ready(function () {

  // recaptcha handle
  if ($(".g-recaptcha").length) {
    // Работа с виджетом recaptcha
    // 1. Получить ответ гугл капчи
    var captcha = grecaptcha.getResponse();

    // 2. Если ответ пустой, то выводим сообщение о том, что пользователь не прошёл тест.
    // Такую форму не будем отправлять на сервер.
    if (!captcha.length) {
      // Выводим сообщение об ошибке
      $('#recaptchaError').text('* Вы не прошли проверку "Я не робот"');
    } else {
      // получаем элемент, содержащий капчу
      $('#recaptchaError').text('');
    }
  }

  // filter btn toogle
  if ($(".filter-wrap").length) {
    $(".filter-wrap .btn-filter").on('click', function () {
      $(this).parent().find(".btn-input-filter").click();
    });
  }

});